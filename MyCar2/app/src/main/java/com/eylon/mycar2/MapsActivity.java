package com.eylon.mycar2;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Random;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double currLat = 32.671677;
    private double currLng = 35.195678;
    private Marker _marker;
    private boolean _stopBWasNotPressed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        _stopBWasNotPressed = true;

        // Hide the Stop button.
        Button stopB = (Button) findViewById(R.id.stopB);
        stopB.setVisibility(View.GONE);
        // Hide the Reset button.
        Button resetB = (Button) findViewById(R.id.resetB);
        resetB.setVisibility(View.GONE);
        // Setting my home coordinates.
        LatLng home = new LatLng(currLat, currLng);
        // Setting the icon instead of the default marker.
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.car_icon);
        // Add a marker in my home location.
        _marker = mMap.addMarker(new MarkerOptions().position(home).icon(icon));
        // Calling the function that zooms the camera close to the marker location.
        moveToCurrentLocation(home);

        // Show the Start button.
        Button startB = (Button) findViewById(R.id.startB);
        startB.setVisibility(View.VISIBLE);
        // Waiting the "Start" button to click.
        startB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Hide the Start button.
                Button startB = (Button) findViewById(R.id.startB);
                startB.setVisibility(View.GONE);
                // Show the Stop button.
                final Button stopB = (Button) findViewById(R.id.stopB);
                stopB.setVisibility(View.VISIBLE);

                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        while (_stopBWasNotPressed) {
                            try {
                                Thread.sleep(1000); // Making the program wait a second until it continues.
                            } catch (InterruptedException e) {
                                e.printStackTrace(); // This function throws exception if the Thread.sleep isn't working.
                            }
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    startMoving();
                                }
                            });
                        }
                    }
                };

                Thread myThread = new Thread(runnable); // Creating a thread for the marker movement.
                myThread.start();
            }
        });
    }

    // Function that zooms the camera close to the marker location.
    private void moveToCurrentLocation(LatLng currentLocation)
    {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 1000, null);


    }

    // This func generates a random number to use as a direction.
    public int generateRandomDirection(){
        final int min = 1;
        final int max = 8;
        final int random = new Random().nextInt((max - min) + 1) + min;
        return random;
    }

    // This func makes the new location and sends it to the animateMarker func.
    public void startMoving(){
        int directionNumber = generateRandomDirection();
        final LatLng toPos;
        switch(directionNumber) {
            case 1:
                toPos = new LatLng((currLat + 0.01), currLng); // North.
                // Changes the marker position to this location.
                _marker.setPosition(toPos);
                // Updating the coordinate location.
                currLat = (currLat + 0.01);
                break;
            case 2:
                toPos = new LatLng((currLat - 0.01), currLng); // South.
                _marker.setPosition(toPos);
                currLat = (currLat - 0.01);
                break;
            case 3:
                toPos = new LatLng(currLat, (currLng + 0.01)); // East.
                _marker.setPosition(toPos);
                currLng = (currLng + 0.01);
                break;
            case 4:
                toPos = new LatLng((currLat + 0.01), (currLng + 0.01)); // North east.
                _marker.setPosition(toPos);
                currLat = (currLat + 0.01);
                currLng = (currLng + 0.01);
                break;
            case 5:
                toPos = new LatLng((currLat - 0.01), (currLng + 0.01)); // South east.
                _marker.setPosition(toPos);
                currLat = (currLat - 0.01);
                currLng = (currLng + 0.01);
                break;
            case 6:
                toPos = new LatLng((currLat + 0.01), (currLng - 0.01)); // North west.
                _marker.setPosition(toPos);
                currLat = (currLat + 0.01);
                currLng = (currLng - 0.01);
                break;
            case 7:
                toPos = new LatLng((currLat - 0.01), (currLng - 0.01)); // South west.
                _marker.setPosition(toPos);
                currLat = (currLat - 0.01);
                currLng = (currLng - 0.01);
                break;
            default:
                toPos = new LatLng(currLat, (currLng - 0.01)); // West.
                _marker.setPosition(toPos);
                currLng = (currLng - 0.01);
                break;
        }
    }

    // When the "Stop" button clicks this function starts.
    public void stopButtonClick(View view) {
        _stopBWasNotPressed = false; // Stops the while loop.
        // Hide the Stop button.
        Button stopB = (Button) findViewById(R.id.stopB);
        stopB.setVisibility(View.GONE);
        // Show the Reset button.
        final Button resetB = (Button) findViewById(R.id.resetB);
        resetB.setVisibility(View.VISIBLE);
        // Waiting the "Reset" button to click.
        resetB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _marker.setVisible(false);
                // Setting my home coordinates again.
                currLat = 32.671677;
                currLng = 35.195678;
                // Calling the starting function again.
                onMapReady(mMap);
            }
        });
    }
}